import React from 'react';

import { DigIOResume } from './DigIOResume';
import { EliizaResume } from './EliizaResume';

export const RESUME_THEMES = ['default', 'digio', 'eliiza'];

export function Resume({ data, theme = 'default' }) {
  if (theme.toLowerCase() === 'default') return <DigIOResume data={data} />;
  if (theme.toLowerCase() === 'digio') return <DigIOResume data={data} />;
  if (theme.toLowerCase() === 'eliiza') return <EliizaResume data={data} />;
  return null;
}
