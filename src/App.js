import React, { Component } from 'react';
import Form from 'react-jsonschema-form';

import resumeSchema from './fixtures/schema.json';
import initialResumeData from './fixtures/resume.json';
import { Resume, RESUME_THEMES } from './Resume';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      schema: resumeSchema,
      formData: initialResumeData,
      previewTheme: RESUME_THEMES[0],
    };
    this.updateFormData = this.updateFormData.bind(this);
    this.switchTheme = this.switchTheme.bind(this);
  }

  updateFormData(newData) {
    this.setState({ formData: newData });
  }

  switchTheme() {
    const currentIdx = RESUME_THEMES.indexOf(this.state.previewTheme);
    const isLast = currentIdx === RESUME_THEMES.length - 1;
    const nextIdx = isLast ? 0 : currentIdx + 1;
    const nextTheme = RESUME_THEMES[nextIdx];
    this.setState({ previewTheme: nextTheme });
  }

  render() {
    return (
      <div style={{ display: 'flex' }}>
        <div style={{ flex: 1 }}>
          <h1>Edit resume Details</h1>
          <Form
            schema={resumeSchema}
            formData={this.state.formData}
            onChange={({ formData }) => this.updateFormData(formData)}
          />
        </div>
        <div style={{ flex: 2 }}>
          <h1>
            Resume Preview ({this.state.previewTheme})
            <small>
              <button onClick={this.switchTheme}>next theme</button>
            </small>
          </h1>
          <Resume data={this.state.formData} theme={this.state.previewTheme} />
        </div>
      </div>
    );
  }
}

export default App;
